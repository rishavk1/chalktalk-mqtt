function() {
    /*
    MQTT over websockets

    Steps for connecting the chalktalk sketch to the MQTT server
    1. Create a client object with the same IP address and port number(11883) as the MQTT server
    2. Create a message object with the "destinationname set as the same topic (tag/networktest)"
    3. While sending a message call the object and reassign message with the intended string and destinationname
    4. Do not call client to connect repeatedly(for example calling client.connect() in this.render() ) or none of the messsages will be sent. Call client.connect()
       only once while starting the sketch.

    => There must be a mqttws31.js in the lib dir and an import to that file in index.html. If you have just downloaded the sketch either 
       perform the forementioned tasks or git clone the gitlab repo.
    */
    this.label = 'Area';
    var client = new Paho.MQTT.Client("192.168.1.4", 8080, "clientid")
    client.onMessageArrived = onMessageArrived;
    var mssg = 0;
 


    this.onSwipe[2] = ["Connect to the network", function() {
     
        try{
            
            client.connect({
                onSuccess: onConnect
            })
           
        }
        catch(e)
        {
            console.log(e);
        }
        }];
      
 

    function onConnect() {

        console.log("Connected");
        client.subscribe('tag/message');

    }
    function onMessageArrived(message) {
        console.log("hello")
        if(message.destinationName == "tag/message")
        {   
            console.log(message.payloadString)
            mssg = parseInt(message.payloadString)
        }
        
      }
    this.render = function() {
       

        mDrawRect([0,0],[4,3])
        textHeight(this.mScale(0.4));


        
        if(mssg == 2)
        {
            mText("3",[-0.5,1.5])
            mText("4",[2,-0.5])
            mText("Height",[-2,1.5])
            mText("base",[2.4,-0.45])
        }
        if(mssg == 3)
        {
            mText("3",[-0.5,1.5])
            mText("4",[2,-0.5])
            mText("Height",[-2,1.5])
            mText("base",[2.4,-0.45])
            color("red")
            mLine([-0.4,1.2],[4,-0.2])
            mLine([2.4,-0.8],[4,-0.3])
        }
       if(mssg == 4)
       {   color("yellow")
            mText("3",[-0.5,1.5])
            mText("4",[2,-0.5])
            color("white")
            mText("Height",[-2,1.5])
            mText("base",[2.4,-0.45])
            mText("A = base x height",[0.325,-1.25])
       }
if(mssg == 5)
       {   color("yellow")
            mText("3",[-0.5,1.5])
            mText("4",[2,-0.5])
            color("white")
            mText("Height",[-2,1.5])
            mText("base",[2.4,-0.45])
            mDrawRect([0.1,-1],[5,-3])
            mText("A = base x height",[0.325,-1.25])
            mText("A = 4 x 3",[0.325,-1.8])
            mText("A = 12",[0.325,-2.3])
            color("red")
            mLine([5,0.5],[4.5,-1])
       }
       
        
    }
}